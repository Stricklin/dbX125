USE gv;

CREATE TABLE factory
	(fac_id int,
	fac_name varchar(40),
	address varchar(80),
	zip_code int(9),
	phone varchar(15),
	manager varchar(40),
	primary key (fac_id)
	);
CREATE TABLE material
	(m_code int,
	m_name varchar(40),
	quantity int,
	unit varchar(20),
	min_level int,
	primary key (m_code)
	);
CREATE TABLE product
	(p_code int,
	p_name varchar(40),
	description varchar(1000),
	quantity int,
	unit varchar(20),
	avg_cost float,
	primary key (p_code)
	);
CREATE TABLE supplier
	(sup_id int,
    sup_name varchar(50),
	website varchar(40),
	contact_email varchar(40),
	primary key (sup_id)
	);
CREATE TABLE customer
	(cus_id int,
    cus_name varchar(50),
	contact_person varchar(40),
	contact_email varchar(40),
	primary key (cus_id)
	);
CREATE TABLE contract
	(contract_id int,
	cus_id int,
	date DATE,
	sale_amount float,
	pay_schedule varchar(40),
	primary key (contract_id),
	foreign key (cus_id) references customer (cus_id)
	);
CREATE TABLE purchase
	(purchase_num int,
	sup_id int,
	sup_order_num int,
	book_date DATE,
	pay_date DATE,
	note varchar(1000),
	primary key (purchase_num),
	foreign key (sup_id) references supplier (sup_id)
	);
CREATE TABLE makes 
	(fac_id int,
	p_code int,
	quantity int,
	primary key (fac_id, p_code),
	foreign key (fac_id) references factory (fac_id),
	foreign key (p_code) references product (p_code)
	);
CREATE TABLE lineitem
	(contract_id int,
	p_code int,
	quantity int,
	primary key (contract_id, p_code),
	foreign key (contract_id) references contract (contract_id),
	foreign key (p_code) references product (p_code)
	);
CREATE TABLE purchaseline
	(purchase_num int,
	m_code int,
	quantity int,
	primary key (purchase_num, m_code),
	foreign key (purchase_num) references purchase (purchase_num),
	foreign key (m_code) references material (m_code)
	);
CREATE TABLE account_payable
	(sup_id int,
    balance float,
	primary key (sup_id),
	foreign key (sup_id) references supplier (sup_id)
	);
CREATE TABLE account_receivable
	(cus_id int,
    balance float,
	primary key (cus_id),
	foreign key (cus_id) references customer (cus_id)
	);
	
/* tables taken/modified from LD */

CREATE TABLE worker
	(work_id int, 
	name varchar (20),
	address varchar (50),
	zip_code int,
	email varchar (40), 
	gender char(1),
	primary key (work_id)
	);
CREATE TABLE phone_num
	(work_id int,
	phone_num varchar(15),
	primary key (work_id, phone_num),
	foreign key (work_id) references worker (work_id)
	);
CREATE TABLE position
	(pos_code int, 
	title varchar (100), 
	description varchar (1000), 
	pay_range_high float, 
	pay_range_low float,
	primary key (pos_code)
	);
CREATE TABLE job
	(job_code int,
	pos_code int,
	emp_mode varchar(9)
		check (emp_mode in ('full-time', 'part-time')),
	pay_rate float,
	pay_type varchar(6)
		check (pay_type in ('salary', 'hourly')),
	cate_code varchar (50),
	fac_id int,
	primary key (job_code),
	foreign key (pos_code) references position (pos_code),
	foreign key (fac_id) references factory (fac_id)
	);
CREATE TABLE skill
	(sk_code int, 
	title varchar (100), 
	description varchar (1000), 
	level varchar(8)
		check (level in ('beginner','medium','advanced')),
	primary key (sk_code)
	);
CREATE TABLE course
	(c_code int,
	title varchar (50), 
	level varchar(8)
		check (level in ('beginner','medium','advanced')), 
	description varchar (1000),
	status varchar(8)
		check (status in ('active', 'expired')), 
	retail_price float,
	primary key (c_code)
	);
CREATE TABLE section
	(c_code int,
    sec_no int,
	complete_date date, 
	year year, 
	offered_by varchar (20), 
	format varchar(18)
		check (format in ('classroom', 'online-sync', 'online-self-paced', 'correspondence')), 
	price float,
	primary key (c_code, sec_no, complete_date),
	foreign key (c_code) references course (c_code)
	);
CREATE TABLE has_skill
	(work_id int,
	sk_code int,
	primary key (work_id, sk_code),
	foreign key (work_id) references worker (work_id),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE works
	(work_id int,
	job_code int,
	start_date date,
	end_date date,
	primary key (work_id, job_code),
	foreign key (work_id) references worker (work_id),
	foreign key (job_code) references job (job_code)
	);
CREATE TABLE requires_by_job
	(job_code int,
	sk_code int,
	primary key (job_code, sk_code),
	foreign key (job_code) references job (job_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE requires
	(pos_code int,
	sk_code int,
	primary key (pos_code, sk_code),
	foreign key (pos_code) references position (pos_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE teaches
	(c_code int,
	sk_code int,
	primary key (c_code, sk_code),
	foreign key (c_code) references course (c_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE takes
	(work_id int,
	c_code int, 
	sec_no int, 
	complete_date date,
	primary key (work_id, c_code, sec_no, complete_date),
	foreign key (work_id) references worker (work_id),
	foreign key (c_code, sec_no, complete_date) references section (c_code, sec_no, complete_date)
	);
CREATE TABLE prerequisite
	(c_code int,
	required_code int,
	primary key (c_code, required_code),
	foreign key (c_code) references course (c_code),
	foreign key (required_code) references course (c_code)
	);