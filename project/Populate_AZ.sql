USE az;

INSERT INTO skill VALUES (1, 'People Management', 'the process of training, motivating and directing employees in order to optimize workplace productivity and promote professional growth.', 'beginner');
INSERT INTO skill VALUES (2, 'Analytical Reasoning', 'the ability to look at information, be it qualitative or quantitative in nature, and discern patterns within the information.', 'beginner');
INSERT INTO skill VALUES (3, 'Mobile Application Development', 'process by which a mobile app is developed for mobile devices, such as personal digital assistants, enterprise digital assistants or mobile phones.', 'beginner');
INSERT INTO skill VALUES (4, 'Sales Leadership', 'Sales leadership is about maximizing opportunities others don''t see.', 'beginner');
INSERT INTO skill VALUES (5, 'Game Development', 'the art of creating games and describes the design, development and release of a game.', 'beginner');
INSERT INTO skill VALUES (6, 'Social Media Marketing', 'the use of social media websites and social networks to market a company''s products and services.', 'beginner');
INSERT INTO skill VALUES (7, 'Journalism', 'a form of writing that tells people about things that really happened, but that they might not have known about already.', 'beginner');
INSERT INTO skill VALUES (8, 'Digital Marketing', 'the component of marketing that utilizes internet and online based digital technologies such as desktop computers, mobile phones and other digital media and platforms to promote products and services', 'beginner');
INSERT INTO skill VALUES (9, 'Customer Service Systems', 'Customer service involves being a kind, courteous, and professional face for the company.', 'beginner');
INSERT INTO skill VALUES (10, 'Corporate Communications', 'the practice of developing, cultivating and maintaining a corporate identity or brand image.', 'beginner');
INSERT INTO skill VALUES (11, 'Project Management', ' the practice of initiating, planning, executing, controlling, and closing the work of a team to achieve specific goals and meet specific success criteria at the specified time.', 'medium');
INSERT INTO skill VALUES (12, 'Competitive Strategies', 'the long term plan of a particular company in order to gain competitive advantage over its competitors in the industry.', 'medium');
INSERT INTO skill VALUES (13, 'UX Design', 'UX design focuses on the interaction between real human users (like you and me) and everyday products and services, such as websites, apps, and even coffee machines. It''s an extremely varied discipline, combining aspects of psychology, business, market research, design, and technology.', 'medium');
INSERT INTO skill VALUES (14, 'Video Production', 'the process of producing video content. It is the equivalent of filmmaking, but with images recorded digitally instead of on film stock.', 'medium');
INSERT INTO skill VALUES (15, 'Audio Production', 'Audio production is when someone records audio and edits it to make it sound a certain way.', 'medium');
INSERT INTO skill VALUES (16, 'Scientific Computing', 'The use of computing to solve scientific and engineering problems, especially by means of simulation, or the construction of mathematical models of physical, chemical or biological processes.', 'medium');
INSERT INTO skill VALUES (17, 'SQL', 'SQL is the main language that allows your database servers to store and edit the data on it.', 'medium');
INSERT INTO skill VALUES (18, 'Business Analysis', 'business analysis is the discipline of recognizing business needs and findings solutions to various business problems.', 'medium');
INSERT INTO skill VALUES (19, 'Software Testing', 'Software testing is a process, to evaluate the functionality of a software application with an intent to find whether the developed software met the specified requirements or not and to identify the defects to ensure that the product is defect-free in order to produce the quality product.', 'medium');
INSERT INTO skill VALUES (20, 'Computer Graphics', 'combines skills in technology, business and art, using computer-generated images and words to create unique concepts and messages for publishing and advertising.', 'medium');
INSERT INTO skill VALUES (21, 'Statistical Software', 'Statistical software are specialized computer programs which help you to collect, organize, analyze, interpret and statistically design data. There are two main statistical techniques which help in statistical data analysis: descriptive statistics and inferential statistics.', 'advanced');
INSERT INTO skill VALUES (22, 'Artificial Intelligence', 'the ability of a computer or a robot controlled by a computer to do tasks that are usually done by humans because they require human intelligence and discernment.', 'advanced');
INSERT INTO skill VALUES (23, 'Translation', 'translating spoken word or text from one language to another.', 'advanced');
INSERT INTO skill VALUES (24, 'Natural Language Processing', 'a subfield of linguistics, computer science, information engineering, and artificial intelligence concerned with the interactions between computers and human (natural) languages, in particular how to program computers to process and analyze large amounts of natural language data.', 'advanced');
INSERT INTO skill VALUES (25, 'Industrial Design', 'process of design applied to products that are to be manufactured through techniques of mass production.', 'advanced');
INSERT INTO skill VALUES (26, 'Data Science', 'the concept to unify statistics, data analysis, machine learning and their related methods in order to understand and analyze actual phenomena with data. It uses techniques and theories drawn from many fields within the context of mathematics, statistics, computer science, and information science.', 'advanced');
INSERT INTO skill VALUES (27, 'Cloud Computing', 'type of computing that relies on shared computing resources rather than having local servers or personal devices to handle applications.', 'advanced');
INSERT INTO skill VALUES (28, 'Information Systems Design', 'The systems development life cycle (SDLC) is an approach for designing and developing MIS solutions. It proceeds in stages: analysis, requirements (vision of future state), design, development, and implementation. Information systems professionals often make the equivalent of a sketch of the design of the final system.', 'advanced');
INSERT INTO skill VALUES (29, 'Software Engineering', ' the process of analyzing user needs and designing, constructing, and testing end user applications that will satisfy these needs through the use of software programming languages. It is the application of engineering principles to software development.', 'advanced');
INSERT INTO skill VALUES (30, 'CompTIA Security+', 'Certification that validates that you have the following skills: Detect various types of compromise and understand penetration testing and vulnerability scanning concepts. Install, configure, and deploy network components while assessing and troubleshooting issues to support organizational security. Implement secure network architecture concepts and systems design. Install and configure identity and access services, as well as management controls. Implement and summarize risk management best practices and the business impact. Install and configure wireless security settings and implement public key infrastructure', 'advanced');

INSERT INTO position VALUES (1, 'Systems Engineer', 'This position will be performing database programming, programming changes to the customers third party dashboard software and Analytics Software applications along with other third-party applications.', 141041, 99410);
INSERT INTO position VALUES (2, 'Data Manager', 'Responsible for the day-to-day data management activities', 120000, 78000);
INSERT INTO position VALUES (3, 'DevOps Engineer', 'Works with developers and the IT staff to oversee the code releases.', 129129, 94811);
INSERT INTO position VALUES (4, 'Information Security System Administrator', 'A security systems administrator handles all aspects of information security and protects the virtual data resources of a company. They are responsible for desktop, mobile, and network security, and are also responsible for installing, administering and troubleshooting an organization''s security solutions.', 105931, 78614);
INSERT INTO position VALUES (5, 'Application Developer', 'Someone who creates, tests and programs applications software for computers.', 130000, 75000);
INSERT INTO position VALUES (6, 'Software Developer', 'You''ll be playing a key role in the design, installation, testing and maintenance of software systems. The programs you create are likely to help businesses be more efficient and provide a better service.', 72000, 57000);
INSERT INTO position VALUES (7, 'IT Support Analyst', ' Support analysts provide user support to resolve issues with computer programs, hardware, and peripherals.', 117082, 88632);
INSERT INTO position VALUES (8, 'Controversy Detector', 'Judges fake news.', 90000, 68000);
INSERT INTO position VALUES (9, 'Cyber Security Specialist', 'Cyber Security Specialist is responsible for providing security during the development stages of software systems, networks and data centers. The professionals have to search for vulnerabilities and risks in hardware and software. They manage and monitor any attacks and intrusions.', 152540, 121040);
INSERT INTO position VALUES (10, 'Pickle Jar Opener', 'Needs a strong grip.', 75000, 60000);

INSERT INTO requires VALUES (1, 17);
INSERT INTO requires VALUES (1, 29);
INSERT INTO requires VALUES (1, 13);
INSERT INTO requires VALUES (2, 17);
INSERT INTO requires VALUES (2, 26);
INSERT INTO requires VALUES (3, 27);
INSERT INTO requires VALUES (3, 29);
INSERT INTO requires VALUES (3, 11);
INSERT INTO requires VALUES (4, 28);
INSERT INTO requires VALUES (4, 1);
INSERT INTO requires VALUES (4, 30);
INSERT INTO requires VALUES (4, 11);
INSERT INTO requires VALUES (5, 19);
INSERT INTO requires VALUES (5, 29);
INSERT INTO requires VALUES (5, 13);
INSERT INTO requires VALUES (6, 29);
INSERT INTO requires VALUES (6, 11);
INSERT INTO requires VALUES (6, 19);
INSERT INTO requires VALUES (7, 9);
INSERT INTO requires VALUES (7, 1);
INSERT INTO requires VALUES (7, 2);
INSERT INTO requires VALUES (8, 1);
INSERT INTO requires VALUES (8, 2);
INSERT INTO requires VALUES (8, 3);
INSERT INTO requires VALUES (9, 30);
INSERT INTO requires VALUES (9, 29);
INSERT INTO requires VALUES (9, 28);
INSERT INTO requires VALUES (9, 2);
INSERT INTO requires VALUES (10, 1);
INSERT INTO requires VALUES (10, 2);
INSERT INTO requires VALUES (10, 3);

INSERT INTO store VALUES (25503010, '5 Williamsburg Rd Boyce, New Orleans, LA', 71409,'3184871875');
INSERT INTO store VALUES (25503020, '6554 Fox Pointe, Washington, MI', 48094, '5862323568');
INSERT INTO store VALUES (25504010, '41 Meadow Creek Blvd, Whiteland, IN', 46184, '3175352347');
INSERT INTO store VALUES (25504020, '7283 W Timberleaf Dr, Tucson, AZ', 85757, '5208831915');
INSERT INTO store VALUES (25504030, '4097 Pine Creek Rd SW #6, Grandville, MI', 49418, '6167727341');

INSERT INTO job VALUES (217325615, 1, 25503010, 'full-time', 98000,  'salary', 'Systems Software');
INSERT INTO job VALUES (235519428, 2, 25503010, 'full-time', 106000, 'salary', 'Health Care Technology');
INSERT INTO job VALUES (217157394, 3, 25504010, 'full-time', 87635,  'salary', 'IT Consulting & Other Services');
INSERT INTO job VALUES (26192340, 4,  25504020, 'full-time', 92200,  'salary', 'Application Software');
INSERT INTO job VALUES (217580482, 5, 25504030, 'full-time', 133580, 'salary', 'IT Consulting & Other Services');

INSERT INTO requires_by_job VALUES (217325615, 2);
INSERT INTO requires_by_job VALUES (217325615, 11);
INSERT INTO requires_by_job VALUES (217325615, 29);
INSERT INTO requires_by_job VALUES (235519428, 26);
INSERT INTO requires_by_job VALUES (235519428, 21);
INSERT INTO requires_by_job VALUES (235519428, 10);
INSERT INTO requires_by_job VALUES (217157394, 10);
INSERT INTO requires_by_job VALUES (217157394, 30);
INSERT INTO requires_by_job VALUES (217157394, 11);
INSERT INTO requires_by_job VALUES (26192340, 2);
INSERT INTO requires_by_job VALUES (26192340, 29);
INSERT INTO requires_by_job VALUES (217580482, 30);
INSERT INTO requires_by_job VALUES (217580482, 11);
INSERT INTO requires_by_job VALUES (217580482, 2);

INSERT INTO course VALUES (111111, 'Intro to Software Design', 'beginner', 'Teaches students how to efficiently think like a software engineer.', 'active', 123.45);
INSERT INTO course VALUES (121212, 'CompTIA Security+ Certification', 'advanced', 'CompTIA Security+ is the first security certification IT professionals should earn. It establishes the core knowledge required of any cybersecurity role and provides a springboard to intermediate-level cybersecurity jobs.', 'active', 349.00);
INSERT INTO course VALUES (434343, 'Business 101', 'beginner', 'Beginner''s business course', 'active', 678.90);
INSERT INTO course VALUES (616123, 'Business 102', 'medium', 'A more intermediate business course', 'expired', 750);
INSERT INTO course VALUES (909090, 'Intro to Game Development', 'beginner', 'An introduction course to the process of game design and development', 'active', 805.75);
INSERT INTO course VALUES (123456, 'Digital Design', 'medium', 'An intermediate course for students to learn how to design video and audeo, as well as learning the process of digital marketing', 'active', 1000);
INSERT INTO course VALUES (768901, 'Some Intermediate Programming', 'medium', 'for learning some intermediate programming stuff', 'active', 555);
INSERT INTO course VALUES (654321, 'Some Advanced Programming', 'advanced', 'for learning some advanced programming stuff', 'active', 679.72);
INSERT INTO course VALUES (175573, 'Intro to Industrial Design', 'beginner', '', 'expired', 1111.11);
INSERT INTO course VALUES (333333, 'How to be a Translator', 'advanced', '', 'expired', 750.99);

INSERT INTO prerequisite VALUES (121212, 111111);
INSERT INTO prerequisite VALUES (434343, 121212);
INSERT INTO prerequisite VALUES (616123, 121212);
INSERT INTO prerequisite VALUES (909090, 616123);
INSERT INTO prerequisite VALUES (123456, 111111);
INSERT INTO prerequisite VALUES (768901, 909090);
INSERT INTO prerequisite VALUES (654321, 909090);
INSERT INTO prerequisite VALUES (175573, 121212);
INSERT INTO prerequisite VALUES (333333, 121212);

INSERT INTO section VALUES (111111, 1, '2020-05-15', 2020, 'University', 'classroom', 500);
INSERT INTO section VALUES (121212, 1, '2020-01-09', 2020, 'Some Website', 'online-self-paced', 349);
INSERT INTO section VALUES (434343, 1, '2019-11-02', 2019, 'University', 'classroom', 500);
INSERT INTO section VALUES (434343, 2, '2019-11-02', 2019, 'University', 'classroom', 500);
INSERT INTO section VALUES (616123, 1, '2019-08-28', 2019, 'Random Name Here', 'correspondence', 500);
INSERT INTO section VALUES (616123, 2, '2019-08-28', 2019, 'Random Name Here', 'correspondence', 500);
INSERT INTO section VALUES (909090, 1, '2020-01-18', 2020, 'Anonymous', 'classroom', 500);
INSERT INTO section VALUES (123456, 1, '2020-03-23', 2020, 'Somwhere', 'correspondence', 500);
INSERT INTO section VALUES (654321, 1, '2019-09-21', 2019, 'Somewhere Else', 'classroom', 500);
INSERT INTO section VALUES (654321, 2, '2019-09-21', 2019, 'Somewhere Else', 'online-sync', 500);
INSERT INTO section VALUES (175573, 1, '2019-12-03', 2019, 'University', 'classroom', 500);
INSERT INTO section VALUES (175573, 2, '2019-12-03', 2019, 'University', 'classroom', 500);
INSERT INTO section VALUES (333333, 1, '2019-09-21', 2019, 'University', 'classroom', 500);
INSERT INTO section VALUES (333333, 2, '2019-09-21', 2019, 'University', 'classroom', 500);
INSERT INTO section VALUES (768901, 1, '2020-03-23', 2020, 'Somewhere Else', 'online-sync', 500);

INSERT INTO teaches VALUES (111111, 2);
INSERT INTO teaches VALUES (111111, 29);
INSERT INTO teaches VALUES (111111, 19);
INSERT INTO teaches VALUES (121212, 2);
INSERT INTO teaches VALUES (121212, 30);
INSERT INTO teaches VALUES (434343, 1);
INSERT INTO teaches VALUES (616123, 1);
INSERT INTO teaches VALUES (909090, 1);
INSERT INTO teaches VALUES (123456, 1);
INSERT INTO teaches VALUES (768901, 1);
INSERT INTO teaches VALUES (654321, 1);
INSERT INTO teaches VALUES (175573, 1);
INSERT INTO teaches VALUES (333333, 1);

INSERT INTO worker VALUES (12345, 'Joe Bob', '335 Irving Road, Worthington, OH', 43085, 'joebob@gmail.com', 'm', 217157394);
INSERT INTO worker VALUES (22345, 'Erick Doyle', '3876 Despard Street, Atlanta, GA', 30310, 'edoyle@gmail.com', 'm', 235519428);
INSERT INTO worker VALUES (78901, 'Allison Wilkins', '2166 West Fork Street, Billings, MT', 59101, 'allisonwilkins@gmail.com', 'f', 26192340);
INSERT INTO worker VALUES (97012, 'Eva Terry', '819 McDonald Avenue, Orlando, FL', 32805, 'evaterry@gmail.com', 'f', 235519428);
INSERT INTO worker VALUES (67893, 'Jacob Matthews', '3107 Hart Ridge Road, Bay City, MI', 48708, 'jacobmatthews@gmail.com', 'm', 217325615);

INSERT INTO phone_num VALUES (12345, '1234031234');
INSERT INTO phone_num VALUES (22345, '4043740341');
INSERT INTO phone_num VALUES (78901, '4062011803');
INSERT INTO phone_num VALUES (97012, '4074553194');
INSERT INTO phone_num VALUES (67893, '9893263770');
INSERT INTO phone_num VALUES (12345, '4099854840');
INSERT INTO phone_num VALUES (22345, '8125799278');
INSERT INTO phone_num VALUES (67893, '9378624176');
INSERT INTO phone_num VALUES (67893, '4123297005');

INSERT INTO works VALUES (12345, 217325615, '2016-02-23', '2019-12-06');
INSERT INTO works VALUES (22345, 235519428, '2016-06-20', '2017-07-01');
INSERT INTO works VALUES (78901, 217157394, '2016-04-06', '2018-05-18');
INSERT INTO works VALUES (97012, 26192340, '2016-06-23', CURRENT_DATE);
INSERT INTO works VALUES (67893, 217580482, '2017-03-09', '2019-06-04');

INSERT INTO takes VALUES (12345, 111111, 1, '2020-05-15');
INSERT INTO takes VALUES (22345, 111111, 1, '2020-05-15');
INSERT INTO takes VALUES (78901, 111111, 1, '2020-05-15');
INSERT INTO takes VALUES (97012, 111111, 1, '2020-05-15');
INSERT INTO takes VALUES (67893, 111111, 1, '2020-05-15');
INSERT INTO takes VALUES (12345, 121212, 1, '2020-01-09');
INSERT INTO takes VALUES (22345, 616123, 2, '2019-08-28');
INSERT INTO takes VALUES (97012, 616123, 2, '2019-08-28');
INSERT INTO takes VALUES (78901, 175573, 1, '2019-12-03');
INSERT INTO takes VALUES (78901, 175573, 2, '2019-12-03');
INSERT INTO takes VALUES (97012, 909090, 1, '2020-01-18');
INSERT INTO takes VALUES (67893, 333333, 2, '2019-09-21');
INSERT INTO takes VALUES (12345, 654321, 1, '2019-09-21');
INSERT INTO takes VALUES (22345, 434343, 1, '2019-11-02');
INSERT INTO takes VALUES (67893, 434343, 1, '2019-11-02');

INSERT INTO has_skill VALUES (12345, 17);
INSERT INTO has_skill VALUES (22345, 29);
INSERT INTO has_skill VALUES (78901, 30);
INSERT INTO has_skill VALUES (97012, 2);
INSERT INTO has_skill VALUES (67893, 11);
INSERT INTO has_skill VALUES (12345, 5);
INSERT INTO has_skill VALUES (22345, 13);
INSERT INTO has_skill VALUES (78901, 17);
INSERT INTO has_skill VALUES (97012, 30);
INSERT INTO has_skill VALUES (67893, 29);

INSERT INTO inventory VALUES (45201020, 25503010, 'device', 'Router', 'Device that receive, analyze and move incoming packets to another network', '190', 'lbs', 100, '2018-08-12', '115', 123);
INSERT INTO inventory VALUES (48938989, 25503020, 'device', 'Laptop', 'Computers that you can take everywhere with you without hassle', '225', 'lbs', 200, '2018-03-08', '155', 232);
INSERT INTO inventory VALUES (56868237, 25504010, 'device', 'Tablet', 'Portable computer that uses a touchscreen as its primary input device', 300, 'lbs', 150, '2017-08-12', '255', 323);
INSERT INTO inventory VALUES (32878748, 25504020, 'device', 'Printer', 'Accepts text and graphic output from a computer and transfers the information to paper', 310, 'lbs', 50, '2018-04-20', 350, 239);
INSERT INTO inventory VALUES (36526563, 25504030, 'device', 'keyboard', 'Used to input text, characters, and other commands into a computer', '165', 'lbs', 25, '2018-08-19', 200, 398);

INSERT INTO customer VALUES (23498, 'Lisa Douglas' ,'7400 Jones Dr, Galveston, TX', 77551, 'LDouglas@gmail.com', 'f');
INSERT INTO customer VALUES (34567, 'Donna Bryan', '314 Wade St, Spring Valley, OH', 45370, 'DBryan@gmail.com' , 'm');
INSERT INTO customer VALUES (44550, 'Karen Simpson', '234 W Lee Hwy, New Market, VA', 22844, 'KSimpson@yahoo.com', 'f');
INSERT INTO customer VALUES (43330, 'Brenda Edwards', '2210 Granger Dr, Bridge City, TX', 77611, 'BEdwards@yahoo.com', 'f');
INSERT INTO customer VALUES (54282, 'David Williamson', '90 W Main St B, Stafford Springs,CT', 06076, 'DWilliamson@gmail.com', 'm');

INSERT INTO sales VALUES (78349, 23498, '2019-07-11', 'new');
INSERT INTO sales VALUES (48994, 34567, '2018-05-09', 'new');
INSERT INTO sales VALUES (67989, 44550, '2018-11-11', 'new');
INSERT INTO sales VALUES (78744, 43330, '2018-10-29', 'new');
INSERT INTO sales VALUES (53289, 54282, '2018-10-12', 'new');

INSERT INTO inv_sales VALUES (78349, 45201020, 140, 150);
INSERT INTO inv_sales VALUES (48994, 48938989, 190, 275);
INSERT INTO inv_sales VALUES (67989, 56868237, 240, 200);
INSERT INTO inv_sales VALUES (78744, 32878748, 350, 75);
INSERT INTO inv_sales VALUES (53289, 36526563, 140, 49);
INSERT INTO inv_sales VALUES (53289, 32878748, 100, 80);

INSERT INTO supplier VALUES (238789, 'Bob Works Routers', '4 Thorne Ave. Southfield, MI', 48076, 'network345@gmail.com');
INSERT INTO supplier VALUES (576839, 'Linksys', '23 Broad Street, Andover, MA',  01810, 'link763@yahoo.com');
INSERT INTO supplier VALUES (786754, 'Telnet Links', '90 Meadowbrook St. Wethersfield, CT', 06109, 'tlink3@gmail.com');
INSERT INTO supplier VALUES (984738, 'Biotech Firm Inc.', '23 Broad Street, Andover, MA', 01810, 'biotech89@gmail.com');
INSERT INTO supplier VALUES (878363, 'The Techiest', '626 Birchwood Ave. Ellicott City, MD', 21042, 'tech5@gmail.com');

INSERT INTO purchase VALUES (1234, 238789, '2019-07-11', 'new');
INSERT INTO purchase VALUES (3423, 576839, '2018-05-09', 'new');
INSERT INTO purchase VALUES (4322, 786754, '2018-11-11', 'new');
INSERT INTO purchase VALUES (2345, 786754, '2018-10-29', 'new');
INSERT INTO purchase VALUES (4897, 878363, '2018-10-12', 'like-new');

INSERT INTO purchased_inv VALUES (1234, 45201020, 100, 123);
INSERT INTO purchased_inv VALUES (3423, 48938989, 250, 230);
INSERT INTO purchased_inv VALUES (4322, 56868237, 160, 100);
INSERT INTO purchased_inv VALUES (2345, 32878748, 300, 25);
INSERT INTO purchased_inv VALUES (4897, 36526563, 400, 165);

INSERT INTO account_payable VALUES (238789, 25503010, '100');
INSERT INTO account_payable VALUES (576839, 25503010, '150');
INSERT INTO account_payable VALUES (786754, 25503010, '300');
INSERT INTO account_payable VALUES (984738, 25503010, '390');
INSERT INTO account_payable VALUES (878363, 25504030, '200');

INSERT INTO purchasepaymentrecord VALUES (238789, 1234, '2019-07-11', '100', 'credit');
INSERT INTO purchasepaymentrecord VALUES (576839, 3423, '2018-05-09', '200', 'credit');
INSERT INTO purchasepaymentrecord VALUES (786754, 4322, '2018-11-11', '250', 'credit');
INSERT INTO purchasepaymentrecord VALUES (984738, 2345, '2018-10-29', '300', 'credit');
INSERT INTO purchasepaymentrecord VALUES (878363, 4897, '2018-10-12', '250', 'credit');

INSERT INTO account_receivable VALUES (25503010, 78349);
INSERT INTO account_receivable VALUES (25503020, 48994);
INSERT INTO account_receivable VALUES (25504010, 67989);
INSERT INTO account_receivable VALUES (25504020, 78744);
INSERT INTO account_receivable VALUES (25504030, 53289);






