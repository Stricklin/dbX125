USE gv;

DROP TABLE makes;
DROP TABLE lineitem;
DROP TABLE purchaseline;
DROP TABLE account_payable;
DROP TABLE account_receivable;
DROP TABLE phone_num;
DROP TABLE has_skill;
DROP TABLE works;
DROP TABLE requires_by_job;
DROP TABLE requires;
DROP TABLE teaches;
DROP TABLE takes;
DROP TABLE prerequisite;
DROP TABLE section;
DROP TABLE course;
DROP TABLE skill;
DROP TABLE job;
DROP TABLE worker;
DROP TABLE purchase;
DROP TABLE contract;
DROP TABLE product;
DROP TABLE material;
DROP TABLE supplier;
DROP TABLE customer;
DROP TABLE position;
DROP TABLE factory;
