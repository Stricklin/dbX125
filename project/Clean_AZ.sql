USE az;

DROP TABLE phone_num;
DROP TABLE purchasePaymentRecord; 
DROP TABLE account_payable; 
DROP TABLE account_Receivable; 
DROP TABLE requires; 
DROP TABLE requires_by_job; 
DROP TABLE teaches; 
DROP TABLE prerequisite; 
DROP TABLE takes; 
DROP TABLE has_skill; 
DROP TABLE works; 
DROP TABLE inv_sales; 
DROP TABLE worker; 
DROP TABLE sales; 
DROP TABLE purchased_inv;
DROP TABLE purchase; 
DROP TABLE inventory; 
DROP TABLE supplier; 
DROP TABLE customer; 
DROP TABLE section; 
DROP TABLE course; 
DROP TABLE skill; 
DROP TABLE job; 
DROP TABLE position; 
DROP TABLE store; 