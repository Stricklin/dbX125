/*CREATE DATABASE Az;*/
use az;

CREATE TABLE skill
	(sk_code int, 
	title varchar (100), 
	description varchar (1000), 
	level varchar(8)
		check (level in ('beginner','medium','advanced')),
	primary key (sk_code)
	);
CREATE TABLE position
	(pos_code int, 
	title varchar (100), 
	description varchar (1000), 
	pay_range_high float, 
	pay_range_low float,
	primary key (pos_code)
	);
CREATE TABLE store
	(store_id int,
	address varchar(40),
	zip_code int,
	phone varchar(15),
	primary key (store_id)
	);
CREATE TABLE job
	(job_code int,
    pos_code int,
    store_id int,
	emp_mode varchar(9)
		check (emp_mode in ('full-time', 'part-time')),
	pay_rate float,
	pay_type varchar(6)
		check (pay_type in ('salary', 'hourly')),
	cate_code varchar (50),
	primary key (job_code),
    foreign key (store_id) references store (store_id),
    foreign key (pos_code) references position (pos_code)
	);
CREATE TABLE course
	(c_code int,
	title varchar (50), 
	level varchar(8)
		check (level in ('beginner','medium','advanced')), 
	description varchar (1000),
	status varchar(8)
		check (status in ('active', 'expired')), 
	retail_price float,
	primary key (c_code)
	);
CREATE TABLE section
	(c_code int,
    sec_no int,
	complete_date date, 
	year year, 
	offered_by varchar (20), 
	format varchar(18)
		check (format in ('classroom', 'online-sync', 'online-self-paced', 'correspondence')), 
	price float,
	primary key (c_code, sec_no, complete_date),
	foreign key (c_code) references course (c_code)
	);
CREATE TABLE worker
	(work_id int, 
	name varchar (20),
	address varchar (50),
	zip_code int,
	email varchar (40), 
	gender char(1),
	job_code int,
	primary key (work_id)
	);
CREATE TABLE customer
	(cus_id int,
	name varchar(20),
	address varchar(40),
	zip_code int,
	email varchar(40),
	gender char(1),
	primary key (cus_id)
	);
CREATE TABLE inventory
	(item_num int,
	store_id int,
	sh_title varchar(100),
	title varchar(100),
	description varchar(1000),
	quantity int,
	unit varchar(10),
	avg_cost float,
	old_date date,
	min_level int,
	shelf_code int,
	primary key (item_num, store_id),
	foreign key (store_id) references store(store_id)
	);
CREATE TABLE sales
	(invoice_num int,
    cus_id int,
	date date,
	note varchar(50),
	primary key (invoice_num),
    foreign key (cus_id) references customer (cus_id)
	);
CREATE TABLE supplier
	(sup_id int,
    sup_name varchar (40),
	address varchar (50),
	zip_code int,
	website varchar (40),
    primary key (sup_id)
	);
CREATE TABLE purchase
	(pur_num int,
	sup_id int,
	date date,
	note varchar(50),
	primary key (pur_num),
    foreign key (sup_id) references supplier (sup_id)
	);
CREATE TABLE works
	(work_id int,
	job_code int,
	start_date date,
	end_date date,
	primary key (work_id, job_code),
	foreign key (work_id) references worker (work_id),
	foreign key (job_code) references job (job_code)
	);
CREATE TABLE requires
	(pos_code int,
	sk_code int,
	primary key (pos_code, sk_code),
	foreign key (pos_code) references position (pos_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE requires_by_job
	(job_code int,
	sk_code int,
	primary key (job_code, sk_code),
	foreign key (job_code) references job (job_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE teaches
	(c_code int,
	sk_code int,
	primary key (c_code, sk_code),
	foreign key (c_code) references course (c_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE prerequisite
	(c_code int,
	required_code int,
	primary key (c_code, required_code),
	foreign key (c_code) references course (c_code),
	foreign key (required_code) references course (c_code)
	);
CREATE TABLE takes
	(work_id int,
	c_code int, 
	sec_no int, 
	complete_date date,
	primary key (work_id, c_code, sec_no, complete_date),
	foreign key (work_id) references worker (work_id),
	foreign key (c_code, sec_no, complete_date) references section (c_code, sec_no, complete_date)
	);
CREATE TABLE has_skill
	(work_id int,
	sk_code int,
	primary key (work_id, sk_code),
	foreign key (work_id) references worker (work_id),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE phone_num
	(work_id int,
	phone_num varchar(15),
	primary key (work_id, phone_num),
	foreign key (work_id) references worker (work_id)
	);
CREATE TABLE account_payable 
	(sup_id int, 
	store_id int,
	balance int, 
	primary key (sup_id, store_id), 
	foreign key (sup_id) references supplier (sup_id), 
	foreign key (store_id) references store (store_id)
	);
CREATE TABLE purchasepaymentrecord 
 	(sup_id int,
 	 pur_num int, 
 	 date_type date, 
 	 amount int, 
	 trans_type varchar(6)
		check (trans_type in ('credit', 'debit')),
 	 primary key (sup_id, pur_num), 
 	 foreign key (sup_id) references supplier (sup_id), 
 	 foreign key (pur_num) references purchase (pur_num) 
 	 );
CREATE TABLE account_receivable 
 	(store_id int, 
	 balance int,
 	 primary key (store_id), 
 	 foreign key (store_id) references store (store_id)
 	);
CREATE TABLE purchased_inv 
	(pur_num int, 
	item_num int, 
    quantity int,
    unit_price float,
	primary key (pur_num, item_num), 
	foreign key (pur_num) references purchase (pur_num), 
	foreign key (item_num) references inventory (item_num) 
	);
CREATE TABLE inv_sales 
	(invoice_num int, 
    item_num int, 
    quantity int,
    unit_price float,
	primary key (item_num, invoice_num), 
	foreign key (item_num) references inventory (item_num), 
	foreign key (invoice_num) references sales (invoice_num)
	);