/*CREATE DATABASE ld;*/
use ld;

CREATE TABLE skill
	(sk_code int, 
	title varchar (100), 
	description varchar (1000), 
	level varchar(8)
		check (level in ('beginner','medium','advanced')),
	primary key (sk_code)
	);
CREATE TABLE position
	(pos_code int, 
	title varchar (100), 
	description varchar (1000), 
	pay_range_high float, 
	pay_range_low float,
	primary key (pos_code)
	);
CREATE TABLE job
	(job_code int,
	emp_mode varchar(9)
		check (emp_mode in ('full-time', 'part-time')),
	pay_rate float,
	pay_type varchar(6)
		check (pay_type in ('salary', 'hourly')),
	cate_code varchar (50),
	company varchar (50),
	primary key (job_code)
	);
CREATE TABLE gics
	(ind_group int(4),
	ind_id int(6),
	sub_ind int(8),
	ind_title varchar (40),
	sub_title varchar (40),
	description varchar (1000),
	primary key (sub_ind)
	);
CREATE TABLE company
	(comp_id int,
	comp_title varchar (50),
	address varchar (50),
	zip_code int,
	ind_group int(4), 
	sub_ind int(8), 
	website varchar (40),
	primary key (comp_id),
	foreign key (sub_ind) references gics (sub_ind)
	);
CREATE TABLE course
	(c_code int,
	title varchar (50), 
	level varchar(8)
		check (level in ('beginner','medium','advanced')), 
	description varchar (1000),
	status varchar(8)
		check (status in ('active', 'expired')), 
	retail_price float,
	primary key (c_code)
	);
CREATE TABLE section
	(c_code int,
    sec_no int,
	complete_date date, 
	year year, 
	offered_by varchar (20), 
	format varchar(18)
		check (format in ('classroom', 'online-sync', 'online-self-paced', 'correspondence')), 
	price float,
	primary key (c_code, sec_no, complete_date),
	foreign key (c_code) references course (c_code)
	);
CREATE TABLE person
	(per_id int, 
	name varchar (20),
	address varchar (50),
	zip_code int,
	email varchar (40), 
	gender char(1),
	primary key (per_id)
	);
CREATE TABLE works
	(per_id int,
	job_code int,
	start_date date,
	end_date date,
	primary key (per_id, job_code),
	foreign key (per_id) references person (per_id),
	foreign key (job_code) references job (job_code)
	);
CREATE TABLE requires
	(pos_code int,
	sk_code int,
	primary key (pos_code, sk_code),
	foreign key (pos_code) references position (pos_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE requires_by_job
	(job_code int,
	sk_code int,
	primary key (job_code, sk_code),
	foreign key (job_code) references job (job_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE teaches
	(c_code int,
	sk_code int,
	primary key (c_code, sk_code),
	foreign key (c_code) references course (c_code),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE prerequisite
	(c_code int,
	required_code int,
	primary key (c_code, required_code),
	foreign key (c_code) references course (c_code),
	foreign key (required_code) references course (c_code)
	);
CREATE TABLE takes
	(per_id int,
	c_code int, 
	sec_no int, 
	complete_date date,
	primary key (per_id, c_code, sec_no, complete_date),
	foreign key (per_id) references person (per_id),
	foreign key (c_code, sec_no, complete_date) references section (c_code, sec_no, complete_date)
	);
CREATE TABLE has_skill
	(per_id int,
	sk_code int,
	primary key (per_id, sk_code),
	foreign key (per_id) references person (per_id),
	foreign key (sk_code) references skill (sk_code)
	);
CREATE TABLE job_pos
	(job_code int,
	pos_code int,
	primary key (job_code, pos_code),
	foreign key (job_code) references job (job_code),
	foreign key (pos_code) references position (pos_code)
	);
CREATE TABLE comp_job
	(comp_id int, 
	job_code int,
	primary key (comp_id, job_code),
	foreign key (comp_id) references company (comp_id),
	foreign key (job_code) references job (job_code)
	);
CREATE TABLE phone_num
	(per_id int,
	phone_num varchar(15),
	primary key (per_id, phone_num),
	foreign key (per_id) references person (per_id)
	);